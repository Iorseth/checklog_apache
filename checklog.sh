#!/usr/bin/env bash

OPTIONS=$1
LOGPATH=$2
DATE=$3

http_check() {
# On recupere la première ligne du fichier de log indiqué en argument.
# Chaque argument de cette dernière sera affiché sur une ligne avec le numéro de celle-ci
# Sur chacune d'entre elle, on recupere ce qui commence par les codes retours, tout en mettant une limite
# La ligne récuperée, on affiche la première colomne qui correspondra donc à la place de l'argument dans le fichier de Log

POS=$(head -1 $LOGPATH | sed 's/ /\n/g' | nl |grep -E "20/*|30/*|40/*|50/*" |  egrep -x '.{0,10}' | awk '{print $1}')

cut -d' ' -f $POS $LOGPATH | sort -n | uniq -c 

}

octet_check() {

# On recupere la première ligne du fichier de log indiqué en argument.
# Chaque argument de cette dernière sera affiché sur une ligne avec le numéro de celle-ci
# Sur chacune d'entre elle, on recupere ce qui commence par les codes retours, tout en mettant une limite
# La ligne récuperée, on affiche la première colomne qui correspondra donc à la place de l'argument dans le fichier de Log
# Par défaut, on sait que le nombre de bytes renvoyé est situé juste après la code retour, on va donc incrémenter
# la variable de position qui contenait jusque là ce dernier

POS=$(head -1 $LOGPATH | sed 's/ /\n/g' | nl |grep -E "20/*|30/*|40/*|50/*" |  egrep -x '.{0,10}' | awk '{print $1}')
POS=$((POS+1))

OCTET_RESULT=$(cut -d' ' -f $POS $LOGPATH |awk '{ sum+=$1} END {print sum}')

echo $OCTET_RESULT "octet"

}

ip_check() {
 
echo "nothing"
POS=$(head -1 $LOGPATH
}


main() {
 case $OPTIONS in
 http_check)
     http_check
     ;;
 ip_check)
     ip_check
     ;;
 octet_check)
     octet_check
     ;;

 *)
     echo "option non existante !"
     echo "options disponibles : http_check; ip_check; octet_check"
     ;;
 esac
}

main "@"
